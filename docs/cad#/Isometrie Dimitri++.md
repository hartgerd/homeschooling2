# cad
[bild](https://drive.google.com/file/d/1GIK647X-yuf_EfPgKw8I_SUu9rLauzly/view?usp=drive_link)


## sketcher
[github](https://github.com/hlorus/CAD_Sketcher)
[anleitung](https://hlorus.github.io/CAD_Sketcher/#gh-light-mode-only)



<iframe width="2026" height="730" src="https://www.youtube.com/embed/P4NVfenQc_8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="1730" height="676" src="https://www.youtube.com/embed/1jNDLUDL0gc" title="CAD Modeling In Blender 3.2 | Using CAD Sketcher" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




## moi

[moi-videoanleitung](https://www.youtube.com/playlist?list=PLWewaLaCVOnnDwoCxRTRxrC5-DFQd-YG9)



## fusion360


[fusion360 edu seite](https://www.autodesk.de/education/edu-software/overview?sorting=featured&filters=individual)

<iframe width="1730" height="676" src="https://www.youtube.com/embed/8vIbPC5qp2k?list=PLdTg9Jusx_C74v1rJnj6tCSAhOJv4lbNB" title="Autodesk Education Netzwerklizenzen" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## ONSHAPE

[onshape lehrgang](https://www.youtube.com/watch?v=gloEpUt8_RM&list=PLw48L7HmCgMLBuNFmJOGZYsJtjEEr83jm)
https://www.youtube.com/watch?v=gloEpUt8_RM&list=PLw48L7HmCgMLBuNFmJOGZYsJtjEEr83jm

<iframe width="853" height="480" src="https://www.youtube.com/embed/gloEpUt8_RM?list=PLw48L7HmCgMLBuNFmJOGZYsJtjEEr83jm" title="Onshape Grundkurs - Teil 1 | CAD für 3D-Drucker für Anfänger (Deutsch)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>




## markdown

<iframe width="853" height="480" src="https://www.youtube.com/embed/9rKA62-0Gck" title="Die GRUNDLAGEN von MARKDOWN in EINEM VIDEO und wie man mit PYTHON MARKDOWN in HTML konvertieren kann" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- [Markdown_uni-paderborn](https://hilfe.uni-paderborn.de/GitLab_-_CI/CD_Kurzanleitung_-_statische_Webseiten_mit_HUGO)

- [Markdown_tu-dresden](https://elvis.inf.tu-dresden.de/wiki/index.php/Markdown_-_Eine_%C3%9Cbersicht)
