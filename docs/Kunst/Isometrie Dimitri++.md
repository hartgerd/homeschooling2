# Kunst

## __Die Perspektive__

<iframe width="2026" height="730" src="https://www.youtube.com/embed/oRYhzrZ8G_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="2026" height="730" src="https://www.youtube.com/embed/P4NVfenQc_8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## __blender__

### __nose__
<iframe width="1280" height="650" src="https://www.youtube.com/embed/38gMy3rFbEM" title="Blender Nose Sculpting for Beginners| 5 min" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="1236" height="695" src="https://www.youtube.com/embed/gG7nEKCk4c0" title="How to Sculpt Realistic Nose in Blender in 2 Minutes" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

### __eye__
<iframe width="1236" height="695" src="https://www.youtube.com/embed/DYJdwd-Fgxo" title="Eye Modeling/Sculpt in Blender with Unique way | Easy and Fast" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

<iframe width="1920" height="1080" src="https://www.youtube.com/embed/Bq6sni8u7BQ" title="How to Sculpt Eyes in Blender (Follow Along Tutorial)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

### __mouth__
<iframe width="1920" height="1080" src="https://www.youtube.com/embed/CTq_Djl38FU" title="How To Sculpt Realistic Mouth in Blender in 2.5 Minutes" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

### __ear__

<iframe width="1536" height="864" src="https://www.youtube.com/embed/MWXzfr5suLc" title="How to sculpt an Ear (Visual Anatomy Explained)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

<iframe width="853" height="480" src="https://www.youtube.com/embed/WKfL_NmFUWU" title="Sculpt Ears | EASY Blender Tutorial" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

<iframe width="853" height="480" src="https://www.youtube.com/embed/10J9VaywNBg" title="How To Sculpt Ears in Blender | Follow Along Tutorial" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

<iframe width="1920" height="1080" src="https://www.youtube.com/embed/GHeUm7aNVYs" title="How to create realistic ear|| Create realistic ear in blender #blender #sculpting #3d" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

<iframe width="853" height="480" src="https://www.youtube.com/embed/M_uJKBl9BPY" title="How To Sculpt The Ears in Blender" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

___

## __meetzi__

https://meetzi.de/app.php?v,Schule_fablab,LSFLY4


[meetzi_blender JG 10](https://meetzi.de/app.php?v,Schule_fablab,LSFLY4)

---
<img src="../../images/Kunst/meetzi1.png" width = 900px><br>

---

<img src="../../images/Kunst/meetzi2.png" width = 900px><br>  

---

<img src="../../images/Kunst/meetzi3.png" width = 900px><br>
