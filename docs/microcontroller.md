# microcontroller

## Use Seeeduino XIAO in Kicad PCB Design Like a Components

[Seeeduino XIAO](https://www.seeedstudio.com/blog/2020/04/23/use-seeeduino-xiao-in-kicad-pcb-design-like-a-components/)




## esp32


<iframe width="853" height="480" src="https://www.youtube.com/embed/kNZS-S9tge8" title="DIY-LED-Controller für 5€ (WLED-Tutorial)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>)

<iframe width="853" height="480" src="https://www.youtube.com/embed/8ISalCdWhxs" title="BitBastelei #569 - Die &quot;neuen&quot; ESP32-Varianten" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>)
